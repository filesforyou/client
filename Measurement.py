import requests
import time
from sys import platform as _platform

DEFAULT_INTERVAL = 10
SEND_DATA_THRESHOLD = 6
API_KEY = 'x7Fp9HeepbEyKqcsrJVTjDPEtE6qFDc4'
SECRET_KEY = 'VgqGFwRxavQSs4bwXBKUPE2FtDWF3xpYrJtLbRLzyVmGDLCFgVu7twQyxtHpggJNGbQpDEg76xWJm37VjCEeQNjxsAsvxvnvPtnPJVTcwus6ce5udnSmPHnV2fr4zDuy'
REQUEST_URL = 'https://ancient-brushlands-28663.herokuapp.com/rest/measurements/add'

class Measuremnt():
    def __init__(self, email, interval=DEFAULT_INTERVAL, threshold=SEND_DATA_THRESHOLD):
        self.data = {
            'operatingSystem': self.getOperatingSystem(),
            'lastReceived': None,
            'email': email,
            'data': []
            } # Here will be the measurements that will be sent to the server
        self.interval = self.returnIfInteger(interval, DEFAULT_INTERVAL)
        self.threshold = self.returnIfInteger(threshold, SEND_DATA_THRESHOLD)
        self.headers = {
            'key': API_KEY,
            'secret': SECRET_KEY
        }
        

    #This method checks if the provided interval is an integer. If not it returns default interval which is 10
    def returnIfInteger(self, interval, default):
        try:
            return int(interval)
        except ValueError:
            return default

    def getOperatingSystem(self):
        if _platform == "linux" or _platform == "linux2":
            return "Linux"
        elif _platform == "darwin":
            return "MacOS"
        elif _platform == "win32":
            return "Windows"
        elif _platform == "win64":
            return "Windows 64"
        else:
            return "Unknown"

    # This method is called after a successfull request. It sets data back to an empty list
    def emptyData(self):
        self.data['data'] = []

    # This method is called after the CPU usage was measured and adds the usage and the received attribute
    def appendData(self, usage):
        milliseconds = self.getCurrentTimeInMilliseconds()
        self.data['data'].append({
            'usage': usage,
            'received': milliseconds
        })

        self.data['lastReceived'] = milliseconds

        if len(self.data['data']) == self.threshold:
            self.sendData()

    # This method returns the time in milliseconds 
    def getCurrentTimeInMilliseconds(self):
        return int(round(time.time() * 1000))

    # This method sends the data to the server
    def sendData(self):
        response = requests.post(REQUEST_URL, json=self.data, headers=self.headers).json()
        print(response)
        self.emptyData()

