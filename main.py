import Measurement
import sys
import random
import psutil

customerEmails = [
    'default@email.com',
    'customer@email.com',
    'john@email.com',
    'doe@email.com',
    'tarik@email.com'
]

def startMeasurement(email, intervals, threshold):
    measurement = Measurement.Measuremnt(email, intervals, threshold)
    try:
        while True:
            usage = psutil.cpu_percent(interval=measurement.interval)
            print(usage)
            measurement.appendData(usage)
    except KeyboardInterrupt:
        print('Interrupted')
    

if __name__ == "__main__":
    email = sys.argv[1] if len(sys.argv) > 1 else random.choice(customerEmails)
    intervals = sys.argv[2] if len(sys.argv) > 2 else 'Empty'
    threshold = sys.argv[3] if len(sys.argv) > 3 else 'Empty'
    startMeasurement(email, intervals, threshold)
    
